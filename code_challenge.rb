class CodeChallenge


  def self.check_number_valid
    puts "Please enter 10 digit number"
    number = gets.chomp.to_s #getting number
    if number.nil? || number.length != 10 || number.split('').select {|digit| (digit.to_i == 0 || digit.to_i == 1)}.length > 0
      puts "Please add valid number without 0 and 1"
      check_number_valid #wrong data call again
    else
      @start_time = Time.now()
      create_match_words(number) #data is matches call class method to further process
    end

  end

  def self.create_match_words(number)
    #create one hash for converting integer to string
    matching_letters = {"2" => ["a", "b", "c"], "3" => ["d", "e", "f"], "4" => ["g", "h", "i"], "5" => ["j", "k", "l"], "6" => ["m", "n", "o"], "7" => ["p", "q", "r", "s"], "8" => ["t", "u", "v"], "9" => ["w", "x", "y", "z"]}
    #find the combination of number
    combination = number.chars.map {|digit| matching_letters[digit]}
    total_combination = combination.length - 1
    dictionary = Hash.new {|h, k| h[k] = []}
    #create hash for dictionary word that length contain maximum ten
    file_path = "/home/jibin/Downloads/dictionary.txt"
    File.foreach(file_path) do |word|
      dictionary[word.length] << word.chop.to_s.downcase if word.length < 12
    end
    final_words = []
    combination_words = Hash.new {}
    #create combination words that minimum length contain smallest length of dictionary hash key
    for i in (2..total_combination - 2)
      first_combination = combination[0..i]
      next if first_combination.length < 3
      second_combination = combination[i + 1..total_combination]
      next if second_combination.length < 3
      first_combinations = first_combination.shift.product(*first_combination).map(&:join)
      next if first_combinations.nil?
      second_combinations = second_combination.shift.product(*second_combination).map(&:join)
      next if second_combinations.nil?
      combination_words[i] = [(first_combinations & dictionary[i + 2]), (second_combinations & dictionary[total_combination - i + 1])] # find common value that include dictionary
    end
    #combination data arrangement

    combination_words.each do |key, combinations|
      next if combinations.first.nil? || combinations.last.nil?
      combinations.first.product(combinations.last).each do |combo_words|
        final_words << combo_words
      end
    end

    #checking all data combination single words present or not
    final_words << (combination.shift.product(*combination).map(&:join) & dictionary[11]).join(", ")

    end_time = Time.now()
    puts final_words.inspect
    puts "Total time in seconds #{end_time.to_f - @start_time.to_f}"
  end

end
CodeChallenge.check_number_valid